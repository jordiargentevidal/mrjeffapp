-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         8.0.27 - MySQL Community Server - GPL
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para mrjeff
CREATE DATABASE IF NOT EXISTS `mrjeff` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `mrjeff`;

-- Volcando estructura para tabla mrjeff.customer
CREATE TABLE IF NOT EXISTS `customer` (
  `id` int NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `persona` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Volcando datos para la tabla mrjeff.customer: ~2 rows (aproximadamente)
DELETE FROM `customer`;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` (`id`, `name`, `persona`) VALUES
	(1, 'John Smith', 'JUNIOR'),
	(2, 'Kevin Price', 'SENIOR');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;

-- Volcando estructura para tabla mrjeff.product
CREATE TABLE IF NOT EXISTS `product` (
  `id` int NOT NULL,
  `vertical` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `margin` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Volcando datos para la tabla mrjeff.product: ~4 rows (aproximadamente)
DELETE FROM `product`;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`id`, `vertical`, `description`, `product_type`, `price`, `margin`) VALUES
	(1, 'Laundry', 'Laundry Plan S', 'SUBSCRIPTION', 54.99, 16.49),
	(3, 'Beauty', 'Haircut', 'SERVICE', 9.95, 1.99),
	(4, 'Fit', 'HIIT class', 'SERVICE', 4.99, 0.99),
	(5, 'Fit', 'Fit subscription', 'SUBSCRIPTION', 45, 4.5);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;

-- Volcando estructura para tabla mrjeff.product_score
CREATE TABLE IF NOT EXISTS `product_score` (
  `id` int NOT NULL,
  `product_id` int DEFAULT NULL,
  `score` smallint DEFAULT NULL,
  `persona` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `product_score_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Volcando datos para la tabla mrjeff.product_score: ~8 rows (aproximadamente)
DELETE FROM `product_score`;
/*!40000 ALTER TABLE `product_score` DISABLE KEYS */;
INSERT INTO `product_score` (`id`, `product_id`, `score`, `persona`) VALUES
	(1, 1, 8, 'JUNIOR'),
	(2, 3, 7, 'JUNIOR'),
	(3, 4, 9, 'JUNIOR'),
	(4, 5, 8, 'JUNIOR'),
	(5, 1, 7, 'SENIOR'),
	(6, 3, 9, 'SENIOR'),
	(7, 4, 5, 'SENIOR'),
	(8, 5, 7, 'SENIOR');
/*!40000 ALTER TABLE `product_score` ENABLE KEYS */;

-- Volcando estructura para tabla mrjeff.subscription
CREATE TABLE IF NOT EXISTS `subscription` (
  `id` int NOT NULL,
  `customer_id` int DEFAULT NULL,
  `product_id` int DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `subscription_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`),
  CONSTRAINT `subscription_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Volcando datos para la tabla mrjeff.subscription: ~6 rows (aproximadamente)
DELETE FROM `subscription`;
/*!40000 ALTER TABLE `subscription` DISABLE KEYS */;
INSERT INTO `subscription` (`id`, `customer_id`, `product_id`, `active`) VALUES
	(1, 1, 1, 1),
	(2, 1, 3, 1),
	(3, 1, 4, 0),
	(4, 2, 1, 1),
	(5, 2, 4, 0),
	(6, 2, 5, 1);
/*!40000 ALTER TABLE `subscription` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
