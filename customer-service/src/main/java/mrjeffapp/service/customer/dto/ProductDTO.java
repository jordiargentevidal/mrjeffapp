package mrjeffapp.service.customer.dto;

import lombok.Data;
import mrjeffapp.service.customer.enums.ProductType;

@Data
public class ProductDTO {

    private Long id;
    private String vertical;
    private String description;
    private Double price;
    private ProductType productType;

}
