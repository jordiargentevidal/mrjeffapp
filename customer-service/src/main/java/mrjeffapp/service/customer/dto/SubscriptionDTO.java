package mrjeffapp.service.customer.dto;

import lombok.Data;

@Data
public class SubscriptionDTO {

    private Long id;
    private Boolean active;
    private ProductDTO product;

}
