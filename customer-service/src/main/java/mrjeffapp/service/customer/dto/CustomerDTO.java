package mrjeffapp.service.customer.dto;

import lombok.Data;
import mrjeffapp.service.customer.enums.PersonaCode;

import java.util.List;

@Data
public class CustomerDTO {

    private Long id;
    private String name;
    private PersonaCode personaCode;
    private List<SubscriptionDTO> subscriptions;


}
