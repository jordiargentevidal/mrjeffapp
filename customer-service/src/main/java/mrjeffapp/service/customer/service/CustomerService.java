package mrjeffapp.service.customer.service;

import mrjeffapp.service.customer.entity.Customer;

import java.util.Optional;


public interface CustomerService {

    Optional<Customer> getCustomer(Long id, Boolean onlyActiveSubscriptions);
}