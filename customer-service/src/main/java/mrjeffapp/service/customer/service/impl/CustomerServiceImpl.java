package mrjeffapp.service.customer.service.impl;

import lombok.extern.slf4j.Slf4j;
import mrjeffapp.service.customer.entity.Customer;
import mrjeffapp.service.customer.repository.CustomerRepository;
import mrjeffapp.service.customer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
@Slf4j
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public Optional<Customer> getCustomer(Long id, Boolean onlyActiveSubscriptions) {
        Optional<Customer> customer = customerRepository.findById(id);
        if (onlyActiveSubscriptions && customer.isPresent()) {
            removeInactiveSubscriptions(customer.get());
        }
        return customer;
    }

    private void removeInactiveSubscriptions(Customer customer) {
        customer.getSubscriptions().removeIf(subscription -> !subscription.getActive());
    }

}