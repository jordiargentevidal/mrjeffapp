package mrjeffapp.service.customer.controller;

import lombok.extern.slf4j.Slf4j;
import mrjeffapp.service.customer.dto.CustomerDTO;
import mrjeffapp.service.customer.mapper.CustomerMapper;
import mrjeffapp.service.customer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/customers")
@Slf4j
public class CustomerController {

    private final CustomerService customerService;
    private final CustomerMapper customerMapper;

    @Autowired
    public CustomerController(CustomerService customerService, CustomerMapper customerMapper) {
        this.customerService = customerService;
        this.customerMapper = customerMapper;
    }

    @GetMapping("/{id}")
    public ResponseEntity<CustomerDTO> getCustomer(@PathVariable("id") Long id,
                                                   @RequestParam(name = "onlyActiveSubscriptions", defaultValue = "true") Boolean onlyActiveSubscriptions) {
        log.info("Customer find by: id={}", id);
        return customerService.getCustomer(id, onlyActiveSubscriptions)
                .map(customer -> ResponseEntity.status(HttpStatus.OK).body(customerMapper.toDto(customer)))
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "item not found"));
    }
}
