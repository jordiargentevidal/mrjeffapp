package mrjeffapp.service.customer.entity;

import lombok.Data;
import mrjeffapp.service.customer.enums.ProductType;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "product")
public class Product implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String vertical;

    @Column
    private String description;

    @Column(name = "product_type")
    @Enumerated(EnumType.STRING)
    private ProductType productType;

    @Column
    private Double price;

    @Column
    private Double margin;

}
