package mrjeffapp.service.customer.entity;

import lombok.Data;
import mrjeffapp.service.customer.enums.PersonaCode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Table(name = "customer")
public class Customer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String name;

    @Column(name = "persona")
    @Enumerated(EnumType.STRING)
    private PersonaCode personaCode;

    @OneToMany(mappedBy = "customer")
    private List<Subscription> subscriptions;

}
