package mrjeffapp.service.customer.enums;


public enum ProductType {
    SUBSCRIPTION, SERVICE;
}