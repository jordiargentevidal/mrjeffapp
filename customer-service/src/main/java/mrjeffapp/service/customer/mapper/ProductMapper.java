package mrjeffapp.service.customer.mapper;

import mrjeffapp.service.customer.dto.ProductDTO;
import mrjeffapp.service.customer.entity.Product;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public interface ProductMapper {

    ProductDTO toDto(Product product);

    Product toEntity(ProductDTO productDTO);

}
