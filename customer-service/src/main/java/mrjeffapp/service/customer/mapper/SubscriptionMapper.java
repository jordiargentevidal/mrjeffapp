package mrjeffapp.service.customer.mapper;

import mrjeffapp.service.customer.dto.SubscriptionDTO;
import mrjeffapp.service.customer.entity.Subscription;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring", uses = ProductMapper.class)
public interface SubscriptionMapper {

    SubscriptionDTO toDto(Subscription subscription);

    Subscription toEntity(SubscriptionDTO subscriptionDTO);

}
