package mrjeffapp.service.customer.mapper;

import mrjeffapp.service.customer.dto.CustomerDTO;
import mrjeffapp.service.customer.entity.Customer;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring", uses = SubscriptionMapper.class)
public interface CustomerMapper {

    CustomerDTO toDto(Customer customer);

    Customer toEntity(CustomerDTO customerDTO);

}
