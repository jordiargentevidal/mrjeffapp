package mrjeffapp.service.customer;

import mrjeffapp.service.customer.entity.Customer;
import mrjeffapp.service.customer.entity.Product;
import mrjeffapp.service.customer.entity.Subscription;
import mrjeffapp.service.customer.enums.PersonaCode;
import mrjeffapp.service.customer.service.CustomerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class CustomerTest {

    private static final String PRODUCT_DESCRIPTION = "Haircut";
    private static final String PRODUCT_VERTICAL = "Beauty";
    private static final long SUBSCRIPTION_ID = 1L;
    private static Customer CUSTOMER;
    private static final Long CUSTOMER_ID = 5L;
    private static final Long CUSTOMER_ID_NOT_FOUND = 9L;
    private static final String CUSTOMER_NAME = "Ronald";

    private static final String CUSTOMER_PATH = "/customers/";
    private static final String PATH_GET_CUSTOMER_STATUS_OK = CUSTOMER_PATH + CUSTOMER_ID;
    private static final String PATH_GET_CUSTOMER_STATUS_NOT_FOUND = CUSTOMER_PATH + CUSTOMER_ID_NOT_FOUND;

    @MockBean
    private CustomerService customerService;

    @Autowired
    private MockMvc mockMvc;


    @BeforeEach
    public void init() {
        CUSTOMER = getCustomerEntity();
    }

    @Test
    void shouldGet_CustomerById_ReturnsHttpStatusOk() throws Exception {
        when(customerService.getCustomer(5L, true)).thenReturn(Optional.of(CUSTOMER));
        mockMvc.perform(get(PATH_GET_CUSTOMER_STATUS_OK).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.id").value(CUSTOMER_ID))
                .andExpect(jsonPath("$.name").value(CUSTOMER_NAME))
                .andExpect(jsonPath("$.personaCode").value(PersonaCode.JUNIOR.name()))
                .andExpect(jsonPath("$.subscriptions[0].id").value(SUBSCRIPTION_ID))
                .andExpect(jsonPath("$.subscriptions[0].active").value(true))
                .andExpect(jsonPath("$.subscriptions[0].product.description").value(PRODUCT_DESCRIPTION))
                .andExpect(jsonPath("$.subscriptions[0].product.vertical").value(PRODUCT_VERTICAL));
    }

    @Test
    void shouldGet_CustomerById_ReturnsHttpStatusNotFound() throws Exception {
        mockMvc.perform(get(PATH_GET_CUSTOMER_STATUS_NOT_FOUND).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    private static Customer getCustomerEntity() {
        Product product = new Product();
        product.setDescription(PRODUCT_DESCRIPTION);
        product.setVertical(PRODUCT_VERTICAL);

        Subscription subscriptionActive = new Subscription();
        subscriptionActive.setId(SUBSCRIPTION_ID);
        subscriptionActive.setActive(true);
        subscriptionActive.setProduct(product);

        Customer customer = new Customer();
        customer.setId(CUSTOMER_ID);
        customer.setName(CUSTOMER_NAME);
        customer.setPersonaCode(PersonaCode.JUNIOR);
        customer.setSubscriptions(Arrays.asList(subscriptionActive));

        return customer;
    }

}