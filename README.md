# Mr Jeff Backend App - Test evaluation Jordi Argente

This project is created for the evaluation test for Mr Jeff by Jordi Argente.

## There are three microservices

- **Customer** : This microservice is responsible for managing customers.
- **Product Score** : This microservice is responsible for managing product score.
- **Recommendation** : This microservice is responsible for managing recommendations.

### Customer ###
The microservice has been implemented as stated in the requirements.
An improvement has been added to also be able to obtain subscriptions that are not active, since it is an interesting piece of information to be able to exploit in the future in the recommendations microservice.
If in the */customers/{customerId}?OnlyActiveSubscriptions={boolean}* endpoint the *onlyActiveSubscriptions* field is set to false, we will obtain active and non-active subscriptions.

### Product Score ###

The microservice has been implemented as stated in the requirements.
Scores services with a ttl (Time to live) of 60 minutes have been cached through cache redis.

### Recommendation ###

For the recommendations microservice, in order to find the best products for a specific customer, we have** been inspired by recommendations that other applications such as Netflix or Spotify can make, where there are recommendations by sections (Watch / listen again, Most popular, Because have you seen / heard)**.

In the recommendations endpoint we can choose the following types of recommendation:

- **SCORE**: Obtains a list of products ordered by the average of all scores.
**The objective is to show the best rated products.**
*/recommendations/SCORE/customers/{customerId}*

- **PERSONA**: Obtains a list of products, discarding the products with the client's active subscription and orders the list of products by the average score of the client's PERSONA profile.
**The objective is to show products that can fit the customer profile.**
*/recommendations/PERSONA/customers/{customerId}*

- **HISTORICAL**: Obtains a list of products that the customer has purchased in the past.
**The objective is to buy again some of the products contracted in the past.**
*/recommendations/HISTORICAL/customers/{customerId}*

## EndPoints

| Service       | EndPoint                     | Port    | Method | Description                                      |
| ------------- | -----------------------------| :-----: | :-----:| ------------------------------------------------ |
| Customer      | /customers/{customerId}?onlyActiveSubscriptions={boolean}        | 8081    | GET    | Return detail of specified customer and their subscriptions (can be filtered to get only active subscriptions (default) or all)              |
| Product score      | /scores             | 8082    | GET    | Return details of all scores                    |
| Product score      | /products/{productId}/score?persona={personaCode}        | 8082    | GET    | Returns the details of the score and product by persona (JUNIOR, SENIOR)               |
| Recommendation      | /recommendations/{recommendationType}/customers/{customerId}             | 8083    | GET    | Returns the recommended products for the specified customer and the type of recommendation(SCORE, PERSONA, HISTORICAL)                   |


### Gateways ###

| Service       | Uri + EndPoint                                  |
| ------------- | --------------------------------------- |
| Customer      | http://localhost:8080/customers/{customerId}?onlyActiveSubscriptions={boolean}           | 
| Product score      | http://localhost:8080/scores              |
| Product score      | http://localhost:8080/products/{productId}/score?persona={personaCode}         |
| Recommendation      | http://localhost:8080/recommendations/{recommendationType}/customers/{customerId}              |

URI for gateway : **http://localhost:8080**

## Database

When the docker containers are started, the database, tables and data are automatically created (script hosted at database / schema-mysql-mrjeff.sql).
Analyzing the requirements of the evaluation test, tables and final relations are the following:

![IMAGE_DESCRIPTION](database/uml-database.png)

Each table contains the following data:

### customer ###

![IMAGE_DESCRIPTION](database/customer-data.PNG)

### product ###

![IMAGE_DESCRIPTION](database/product-data.PNG)

### product_score ###

![IMAGE_DESCRIPTION](database/product-score-data.PNG)

### subscription ###

![IMAGE_DESCRIPTION](database/subscription-data.PNG)


## Build & Run

- *>mvn clean package* : to build
- *>docker-compose up* --build : build docker images and containers and run containers
- *>docker-compose stop* : stop the dockerized services
- Each maven module has a Dockerfile.

## VERSIONS

### 1.0.0 SNAPSHOT

- Spring-Boot 2.6.1.RELEASE
- Java 17
- Spring Cloud
- Open Feign (to make calls between microservices)
- Resilience4j (control of possible drops or saturation of services through fallback)
- Spring Boot Gateway
- Spring Boot Netflix Eureka (two peer)
- Lombok
- Mapstruct
- Redis cache (to cache score services (60 minutes of ttl))
- MySQL 8
- OpenApi (to document and test the endpoints)

### Future version
- Implement stack Elasticsearch for logs.
- Implement Zipkin for a distributed tracing system.


