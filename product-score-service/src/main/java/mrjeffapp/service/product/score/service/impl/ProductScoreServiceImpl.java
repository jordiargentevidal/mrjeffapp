package mrjeffapp.service.product.score.service.impl;

import lombok.extern.slf4j.Slf4j;
import mrjeffapp.service.product.score.entity.Score;
import mrjeffapp.service.product.score.enums.PersonaCode;
import mrjeffapp.service.product.score.repository.ProductScoreRepository;
import mrjeffapp.service.product.score.service.ProductScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
@Slf4j
public class ProductScoreServiceImpl implements ProductScoreService {

    private final ProductScoreRepository productScoreRepository;

    @Autowired
    public ProductScoreServiceImpl(ProductScoreRepository productScoreRepository) {
        this.productScoreRepository = productScoreRepository;
    }

    @Cacheable(cacheNames = "scores")
    public List<Score> getAllScores() {
        return productScoreRepository.findAll();
    }

    @Cacheable(cacheNames = "scores")
    public Optional<Score> getScoreByProductIdAndPersonaCode(Long id, PersonaCode personaCode) {
        return productScoreRepository.findScoreByProductIdAndPersonaCode(id, personaCode);
    }
}