package mrjeffapp.service.product.score.repository;

import mrjeffapp.service.product.score.entity.Score;
import mrjeffapp.service.product.score.enums.PersonaCode;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ProductScoreRepository extends JpaRepository<Score, Long> {

    public List<Score> findAll();

    public Optional<Score> findScoreByProductIdAndPersonaCode(Long id, PersonaCode personaCode);

}
