package mrjeffapp.service.product.score.mapper;

import mrjeffapp.service.product.score.dto.ProductDTO;
import mrjeffapp.service.product.score.entity.Product;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public interface ProductMapper {

    ProductDTO toDto(Product product);

    Product toEntity(ProductDTO productDTO);

}
