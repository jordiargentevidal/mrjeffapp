package mrjeffapp.service.product.score.controller;

import lombok.extern.slf4j.Slf4j;
import mrjeffapp.service.product.score.dto.ScoreDTO;
import mrjeffapp.service.product.score.entity.Score;
import mrjeffapp.service.product.score.enums.PersonaCode;
import mrjeffapp.service.product.score.mapper.ScoreMapper;
import mrjeffapp.service.product.score.service.ProductScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.ResponseEntity.status;

@Slf4j
@RestController
public class ProductScoreController {

    private final ProductScoreService productScoreService;
    private final ScoreMapper scoreMapper;

    @Autowired
    public ProductScoreController(ProductScoreService productScoreService, ScoreMapper scoreMapper) {
        this.productScoreService = productScoreService;
        this.scoreMapper = scoreMapper;
    }

    @GetMapping("/scores")
    public ResponseEntity<List<ScoreDTO>> getScores() {
        log.info("Find all scores");
        List<Score> scores = productScoreService.getAllScores();
        if (CollectionUtils.isEmpty(scores)) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, "empty scores");
        }

        return status(HttpStatus.OK)
                .body(scores.stream().map(scoreMapper::toDto)
                        .collect(Collectors.toList()));
    }

    @GetMapping("/products/{id}/score")
    public ResponseEntity<ScoreDTO> getScores(@PathVariable("id") Long id, @RequestParam("persona") PersonaCode personaCode) {
        log.info("Find Score by: productId={} personaCode={}", id, personaCode);

        return productScoreService.getScoreByProductIdAndPersonaCode(id, personaCode)
                .map(score -> ResponseEntity.status(HttpStatus.OK).body(scoreMapper.toDto(score)))
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "item not found"));
    }

}
