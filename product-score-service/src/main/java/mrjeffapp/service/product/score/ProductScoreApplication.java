package mrjeffapp.service.product.score;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@Slf4j
@EnableCaching
@EnableEurekaClient
@SpringBootApplication
public class ProductScoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductScoreApplication.class, args);
    }

}
