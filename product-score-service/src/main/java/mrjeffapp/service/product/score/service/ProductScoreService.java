package mrjeffapp.service.product.score.service;

import mrjeffapp.service.product.score.entity.Score;
import mrjeffapp.service.product.score.enums.PersonaCode;

import java.util.List;
import java.util.Optional;

public interface ProductScoreService {

    List<Score> getAllScores();

    Optional<Score> getScoreByProductIdAndPersonaCode(Long id, PersonaCode personaCode);

}
