package mrjeffapp.service.product.score.enums;

public enum ProductType {
    SUBSCRIPTION, SERVICE;
}
