package mrjeffapp.service.product.score.dto;

import lombok.Data;
import mrjeffapp.service.product.score.enums.ProductType;

@Data
public class ProductDTO {

    private Long id;
    private String vertical;
    private String description;
    private Double price;
    private ProductType productType;

}
