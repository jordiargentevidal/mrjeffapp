package mrjeffapp.service.product.score.mapper;

import mrjeffapp.service.product.score.dto.ScoreDTO;
import mrjeffapp.service.product.score.entity.Score;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring", uses = ProductMapper.class)
public interface ScoreMapper {

    ScoreDTO toDto(Score score);

    Score toEntity(ScoreDTO scoreDTO);

}
