package mrjeffapp.service.product.score.dto;

import lombok.Data;
import mrjeffapp.service.product.score.enums.PersonaCode;

@Data
public class ScoreDTO {

    private Long id;
    private Integer score;
    private PersonaCode personaCode;
    private ProductDTO product;

}
