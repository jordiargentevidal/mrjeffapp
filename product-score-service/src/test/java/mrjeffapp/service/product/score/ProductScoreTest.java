package mrjeffapp.service.product.score;

import mrjeffapp.service.product.score.entity.Product;
import mrjeffapp.service.product.score.entity.Score;
import mrjeffapp.service.product.score.enums.PersonaCode;
import mrjeffapp.service.product.score.enums.ProductType;
import mrjeffapp.service.product.score.service.ProductScoreService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class ProductScoreTest {

    private static Product PRODUCT;
    private static final Long PRODUCT_ID = 5L;
    private static final Long PRODUCT_ID_NOT_FOUND = 9L;
    private static final String PRODUCT_DESCRIPTION = "Haircut";
    private static final String PRODUCT_VERTICAL = "Beauty";

    private static List<Score> SCORES;
    private static Score SCORE_1;
    private static final Long SCORE_1_ID = 1L;
    private static final Integer SCORE_REVIEW_1 = 9;
    private static Score SCORE_2;
    private static final Long SCORE_2_ID = 2L;
    private static final Integer SCORE_REVIEW_2 = 7;

    private static final String PATH_SCORES = "/scores";
    private static final String PATH_GET_SCORE_BY_PRODUCT_AND_PERSONA =
            "/products/" + PRODUCT_ID + "/score?";
    private static final String PATH_GET_SCORE_BY_PRODUCT_AND_PERSONA_NOT_FOUND =
            "/products/" + PRODUCT_ID_NOT_FOUND + "/score?";

    @MockBean
    private ProductScoreService productScoreService;

    @Autowired
    private MockMvc mockMvc;


    @BeforeEach
    public void init() {
        PRODUCT = getProductEntity();
        SCORE_1 = getScore1Entity();
        SCORE_2 = getScore2Entity();
        SCORES = Arrays.asList(SCORE_1, SCORE_2);
    }

    @Test
    void shouldGet_ScoreByProductIdAndPersonaCode_ReturnsHttpStatusOk() throws Exception {
        when(productScoreService.getScoreByProductIdAndPersonaCode(PRODUCT_ID, PersonaCode.SENIOR)).thenReturn(Optional.of(SCORE_1));
        mockMvc.perform(get(PATH_GET_SCORE_BY_PRODUCT_AND_PERSONA)
                        .queryParam("persona", PersonaCode.SENIOR.name())
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.id").value(SCORE_1_ID))
                .andExpect(jsonPath("$.score").value(SCORE_REVIEW_1))
                .andExpect(jsonPath("$.personaCode").value(PersonaCode.SENIOR.name()))
                .andExpect(jsonPath("$.product.id").value(PRODUCT_ID))
                .andExpect(jsonPath("$.product.vertical").value(PRODUCT_VERTICAL))
                .andExpect(jsonPath("$.product.description").value(PRODUCT_DESCRIPTION))
                .andExpect(jsonPath("$.product.productType").value(ProductType.SERVICE.name()));
    }

    @Test
    void shouldGet_ScoreByProductIdAndPersonaCode_ReturnsHttpStatusNotFound() throws Exception {
        mockMvc.perform(get(PATH_GET_SCORE_BY_PRODUCT_AND_PERSONA_NOT_FOUND)
                        .queryParam("persona", PersonaCode.JUNIOR.name())
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void shouldGet_AllScores_ReturnsHttpStatusOk() throws Exception {
        when(productScoreService.getAllScores()).thenReturn(SCORES);
        mockMvc.perform(get(PATH_SCORES).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.[0].id").value(SCORE_1_ID))
                .andExpect(jsonPath("$.[0].score").value(SCORE_REVIEW_1))
                .andExpect(jsonPath("$.[0].personaCode").value(PersonaCode.SENIOR.name()))
                .andExpect(jsonPath("$.[0].product.id").value(PRODUCT_ID))
                .andExpect(jsonPath("$.[0].product.vertical").value(PRODUCT_VERTICAL))
                .andExpect(jsonPath("$.[0].product.description").value(PRODUCT_DESCRIPTION))
                .andExpect(jsonPath("$.[0].product.productType").value(ProductType.SERVICE.name()))
                .andExpect(jsonPath("$.[1].id").value(SCORE_2_ID))
                .andExpect(jsonPath("$.[1].score").value(SCORE_REVIEW_2))
                .andExpect(jsonPath("$.[1].personaCode").value(PersonaCode.JUNIOR.name()))
                .andExpect(jsonPath("$.[1].product.id").value(PRODUCT_ID))
                .andExpect(jsonPath("$.[1].product.vertical").value(PRODUCT_VERTICAL))
                .andExpect(jsonPath("$.[1].product.description").value(PRODUCT_DESCRIPTION))
                .andExpect(jsonPath("$.[1].product.productType").value(ProductType.SERVICE.name()));
    }

    @Test
    void shouldGet_AllScores_ReturnsHttpStatusNoContent() throws Exception {
        mockMvc.perform(get(PATH_SCORES).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    private static Product getProductEntity() {
        Product product = new Product();
        product.setId(PRODUCT_ID);
        product.setDescription(PRODUCT_DESCRIPTION);
        product.setVertical(PRODUCT_VERTICAL);
        product.setProductType(ProductType.SERVICE);

        return product;
    }

    private static Score getScore1Entity() {
        Score score = new Score();
        score.setId(SCORE_1_ID);
        score.setProduct(PRODUCT);
        score.setPersonaCode(PersonaCode.SENIOR);
        score.setScore(SCORE_REVIEW_1);

        return score;
    }

    private static Score getScore2Entity() {
        Score score = new Score();
        score.setId(SCORE_2_ID);
        score.setProduct(PRODUCT);
        score.setPersonaCode(PersonaCode.JUNIOR);
        score.setScore(SCORE_REVIEW_2);

        return score;
    }

}