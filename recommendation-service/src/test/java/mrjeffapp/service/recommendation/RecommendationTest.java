package mrjeffapp.service.recommendation;

import mrjeffapp.service.recommendation.dto.CustomerDTO;
import mrjeffapp.service.recommendation.dto.ProductDTO;
import mrjeffapp.service.recommendation.dto.ScoreDTO;
import mrjeffapp.service.recommendation.dto.SubscriptionDTO;
import mrjeffapp.service.recommendation.enums.PersonaCode;
import mrjeffapp.service.recommendation.enums.ProductType;
import mrjeffapp.service.recommendation.enums.RecommendationType;
import mrjeffapp.service.recommendation.service.RecommendationService;
import mrjeffapp.service.recommendation.service.client.CustomerServiceClient;
import mrjeffapp.service.recommendation.service.client.ProductScoreServiceClient;
import mrjeffapp.service.recommendation.service.impl.RecommendationServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class RecommendationTest {

    private static List<ProductDTO> PRODUCTS_RECOMMENDATION_SCORE;
    private static List<ScoreDTO> SCORES;

    private static ProductDTO PRODUCT_1;
    private static final Long PRODUCT_1_ID = 3L;
    private static final String PRODUCT_1_DESCRIPTION = "Haircut";
    private static final String PRODUCT_1_VERTICAL = "Beauty";
    private static final Double PRODUCT_1_PRICE = 9.95;

    private static ProductDTO PRODUCT_2;
    private static ProductDTO PRODUCT_3;

    private static CustomerDTO CUSTOMER;
    private static final Long CUSTOMER_ID = 1L;


    private static final String PATH_GET_SCORE_RECOMMENDATIONS_BY_CUSTOMER =
            "/recommendations/SCORE/customers/" + CUSTOMER_ID;

    @Mock
    private CustomerServiceClient customerServiceClient;

    @Mock
    private ProductScoreServiceClient productScoreServiceClient;

    @MockBean
    private RecommendationService recommendationService;

    private RecommendationServiceImpl recommendationServiceImpl;

    @Autowired
    private MockMvc mockMvc;


    @BeforeEach
    public void init() {
        CUSTOMER = getCustomerDTO();
        SCORES = getScoresDTO();
        PRODUCTS_RECOMMENDATION_SCORE = List.of(PRODUCT_1);
        recommendationServiceImpl = new RecommendationServiceImpl(customerServiceClient, productScoreServiceClient);
    }

    @Test
    void shouldGet_RecommendationsByTypeAndCustomer_ReturnsHttpStatusOk() throws Exception {
        when(recommendationService.getRecommendationsByTypeAndCustomer(RecommendationType.SCORE, CUSTOMER_ID))
                .thenReturn(PRODUCTS_RECOMMENDATION_SCORE);
        mockMvc.perform(get(PATH_GET_SCORE_RECOMMENDATIONS_BY_CUSTOMER)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$[0].id").value(PRODUCT_1_ID))
                .andExpect(jsonPath("$[0].vertical").value(PRODUCT_1_VERTICAL))
                .andExpect(jsonPath("$[0].description").value(PRODUCT_1_DESCRIPTION))
                .andExpect(jsonPath("$[0].price").value(PRODUCT_1_PRICE))
                .andExpect(jsonPath("$[0].productType").value(ProductType.SERVICE.name()));
    }

    @Test
    void shouldGet_RecommendationsByTypeAndCustomer_ReturnsHttpStatusNoContent() throws Exception {
        mockMvc.perform(get(PATH_GET_SCORE_RECOMMENDATIONS_BY_CUSTOMER)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    void shouldGet_RecommendationsByScoreRank() {
        when(productScoreServiceClient.getScores()).thenReturn(SCORES);
        List<ProductDTO> productDTOS = recommendationServiceImpl.getRecommendationsByTypeAndCustomer(RecommendationType.SCORE, CUSTOMER_ID);
        Assertions.assertNotNull(productDTOS);
        Assertions.assertEquals(productDTOS.get(0), PRODUCT_3);
        Assertions.assertEquals(productDTOS.get(1), PRODUCT_2);
        Assertions.assertEquals(productDTOS.get(2), PRODUCT_1);
    }

    @Test
    void shouldGet_RecommendationsByHistoricalSubscriptions() {
        when(customerServiceClient.getCustomer(CUSTOMER_ID, false)).thenReturn(CUSTOMER);
        List<ProductDTO> productDTOS = recommendationServiceImpl.getRecommendationsByTypeAndCustomer(RecommendationType.HISTORICAL, CUSTOMER_ID);
        Assertions.assertNotNull(productDTOS);
        Assertions.assertEquals(productDTOS.get(0), PRODUCT_2);
        Assertions.assertEquals(productDTOS.get(1), PRODUCT_3);
    }

    @Test
    void shouldGet_RecommendationsByPersona() {
        when(productScoreServiceClient.getScores()).thenReturn(SCORES);
        when(customerServiceClient.getCustomer(CUSTOMER_ID, false)).thenReturn(CUSTOMER);
        List<ProductDTO> productDTOS = recommendationServiceImpl.getRecommendationsByTypeAndCustomer(RecommendationType.PERSONA, CUSTOMER_ID);
        Assertions.assertNotNull(productDTOS);
        Assertions.assertEquals(productDTOS.get(0), PRODUCT_2);
        Assertions.assertEquals(productDTOS.get(1), PRODUCT_3);
    }

    private static CustomerDTO getCustomerDTO() {
        PRODUCT_1 = ProductDTO.builder()
                .id(PRODUCT_1_ID)
                .vertical(PRODUCT_1_VERTICAL)
                .description(PRODUCT_1_DESCRIPTION)
                .price(PRODUCT_1_PRICE)
                .productType(ProductType.SERVICE)
                .build();

        SubscriptionDTO subscription1DTO = SubscriptionDTO.builder()
                .id(1L)
                .active(true)
                .product(PRODUCT_1)
                .build();

        PRODUCT_2 = ProductDTO.builder()
                .id(1L)
                .vertical("Laundry")
                .description("Laundry Plan S")
                .price(54.99)
                .productType(ProductType.SUBSCRIPTION)
                .build();

        SubscriptionDTO subscription2DTO = SubscriptionDTO.builder()
                .id(2L)
                .active(false)
                .product(PRODUCT_2)
                .build();

        PRODUCT_3 = ProductDTO.builder()
                .id(4L)
                .vertical("Fit")
                .description("HIIT class")
                .price(4.99)
                .productType(ProductType.SERVICE)
                .build();

        SubscriptionDTO subscription3DTO = SubscriptionDTO.builder()
                .id(3L)
                .active(false)
                .product(PRODUCT_3)
                .build();

        SubscriptionDTO subscription4DTO = SubscriptionDTO.builder()
                .id(4L)
                .active(false)
                .product(PRODUCT_3)
                .build();

        return CustomerDTO.builder()
                .id(CUSTOMER_ID)
                .name("Ronald")
                .personaCode(PersonaCode.JUNIOR)
                .subscriptions(Arrays.asList(subscription1DTO, subscription2DTO, subscription3DTO, subscription4DTO))
                .build();
    }

    private static List<ScoreDTO> getScoresDTO() {
        ScoreDTO score1DTO = ScoreDTO.builder()
                .id(1L)
                .score(6)
                .product(PRODUCT_1)
                .personaCode(PersonaCode.JUNIOR)
                .build();

        ScoreDTO score2DTO = ScoreDTO.builder()
                .id(2L)
                .score(9)
                .product(PRODUCT_2)
                .personaCode(PersonaCode.JUNIOR)
                .build();

        ScoreDTO score3DTO = ScoreDTO.builder()
                .id(3L)
                .score(8)
                .product(PRODUCT_3)
                .personaCode(PersonaCode.JUNIOR)
                .build();

        ScoreDTO score4DTO = ScoreDTO.builder()
                .id(4L)
                .score(6)
                .product(PRODUCT_1)
                .personaCode(PersonaCode.SENIOR)
                .build();

        ScoreDTO score5DTO = ScoreDTO.builder()
                .id(5L)
                .score(7)
                .product(PRODUCT_2)
                .personaCode(PersonaCode.SENIOR)
                .build();

        ScoreDTO score6DTO = ScoreDTO.builder()
                .id(6L)
                .score(9)
                .product(PRODUCT_3)
                .personaCode(PersonaCode.SENIOR)
                .build();

        return Arrays.asList(score1DTO, score2DTO, score3DTO, score4DTO, score5DTO, score6DTO);
    }
}