package mrjeffapp.service.recommendation.controller;

import lombok.extern.slf4j.Slf4j;
import mrjeffapp.service.recommendation.dto.ProductDTO;
import mrjeffapp.service.recommendation.enums.RecommendationType;
import mrjeffapp.service.recommendation.service.RecommendationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static org.springframework.http.ResponseEntity.status;

@Slf4j
@RestController
public class RecommendationController {

    private final RecommendationService recommendationService;

    @Autowired
    public RecommendationController(RecommendationService recommendationService) {
        this.recommendationService = recommendationService;
    }

    @GetMapping("/recommendations/{type}/customers/{customerId}")
    public ResponseEntity<List<ProductDTO>> getRecommendations(@PathVariable("type") RecommendationType recommendationType,
                                                               @PathVariable("customerId") Long customerId) {
        log.info("Find recommendations by: recommendationType={} customerId={}", recommendationType, customerId);
        List<ProductDTO> products = recommendationService.getRecommendationsByTypeAndCustomer(recommendationType, customerId);
        if (CollectionUtils.isEmpty(products)) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, "empty product recommendations");
        }
        return status(HttpStatus.OK).body(products);
    }


}
