package mrjeffapp.service.recommendation.service.impl;


import lombok.extern.slf4j.Slf4j;
import mrjeffapp.service.recommendation.dto.CustomerDTO;
import mrjeffapp.service.recommendation.dto.ProductDTO;
import mrjeffapp.service.recommendation.dto.ScoreDTO;
import mrjeffapp.service.recommendation.dto.SubscriptionDTO;
import mrjeffapp.service.recommendation.enums.RecommendationType;
import mrjeffapp.service.recommendation.service.RecommendationService;
import mrjeffapp.service.recommendation.service.client.CustomerServiceClient;
import mrjeffapp.service.recommendation.service.client.ProductScoreServiceClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
@Transactional
@Slf4j
public class RecommendationServiceImpl implements RecommendationService {

    private final CustomerServiceClient customerServiceClient;
    private final ProductScoreServiceClient productScoreServiceClient;

    @Autowired
    public RecommendationServiceImpl(CustomerServiceClient customerServiceClient,
                                     ProductScoreServiceClient productScoreServiceClient) {
        this.customerServiceClient = customerServiceClient;
        this.productScoreServiceClient = productScoreServiceClient;
    }

    public List<ProductDTO> getRecommendationsByTypeAndCustomer(RecommendationType recommendationType, Long customerId) {
        return switch (recommendationType) {
            case SCORE -> getRecommendationsByScoreRank();
            case HISTORICAL -> getRecommendationsByHistoricalSubscriptions(customerId);
            case PERSONA -> getRecommendationsByPersona(customerId);
        };
    }

    private List<ProductDTO> getRecommendationsByScoreRank() {
        List<ScoreDTO> scoresDTO = productScoreServiceClient.getScores();

        return scoresDTO.stream()
                .collect(Collectors.groupingBy(ScoreDTO::getProduct))
                .values().stream()
                .map(this::calculateAverageOfScoreProduct)
                .sorted(Comparator.comparing(ScoreDTO::getAverageScore).reversed())
                .map(ScoreDTO::getProduct)
                .toList();
    }

    private Predicate<ScoreDTO> filterScoresWithNonActiveSubscription(CustomerDTO customerDTO) {
        return score -> customerDTO.getSubscriptions().stream()
                .filter(subscription -> !subscription.getActive())
                .anyMatch(subscription -> subscription.getProduct().equals(score.getProduct()));
    }

    private ScoreDTO calculateAverageOfScoreProduct(List<ScoreDTO> scoresDTOToAverage) {
        final ScoreDTO scoreDTO = scoresDTOToAverage.iterator().next();
        final double scoreAverage = scoresDTOToAverage.stream()
                .map(score -> Double.valueOf(score.getScore()))
                .collect(Collectors.averagingDouble(Double::doubleValue));
        return ScoreDTO.builder().id(scoreDTO.getId()).score(scoreDTO.getScore()).averageScore(scoreAverage)
                .personaCode(scoreDTO.getPersonaCode()).product(scoreDTO.getProduct())
                .build();
    }

    private List<ProductDTO> getRecommendationsByHistoricalSubscriptions(Long customerId) {
        CustomerDTO customerDTO = customerServiceClient.getCustomer(customerId, false);
        if (customerDTO.getId() == null) {
            return new ArrayList<>();
        }

        return customerDTO.getSubscriptions().stream()
                .filter(subscription -> !subscription.getActive())
                .map(SubscriptionDTO::getProduct)
                .distinct()
                .toList();
    }

    private List<ProductDTO> getRecommendationsByPersona(Long customerId) {
        CustomerDTO customerDTO = customerServiceClient.getCustomer(customerId, false);
        if (customerDTO.getId() == null) {
            return new ArrayList<>();
        }
        List<ScoreDTO> scoresDTO = productScoreServiceClient.getScores();

        return scoresDTO.stream()
                .filter(filterScoresWithNonActiveSubscription(customerDTO)
                        .and(filterByPersonaCode(customerDTO)))
                .collect(Collectors.groupingBy(ScoreDTO::getProduct))
                .values().stream()
                .map(this::calculateAverageOfScoreProduct)
                .sorted(Comparator.comparing(ScoreDTO::getAverageScore).reversed())
                .map(ScoreDTO::getProduct)
                .distinct()
                .toList();
    }

    private Predicate<ScoreDTO> filterByPersonaCode(CustomerDTO customerDTO) {
        return score -> score.getPersonaCode().equals(customerDTO.getPersonaCode());
    }

}