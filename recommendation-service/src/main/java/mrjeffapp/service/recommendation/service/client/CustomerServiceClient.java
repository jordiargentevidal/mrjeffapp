package mrjeffapp.service.recommendation.service.client;

import mrjeffapp.service.recommendation.dto.CustomerDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;


@FeignClient(name = "customer-service", fallback = CustomerServiceClientFallback.class)
public interface CustomerServiceClient {

    @GetMapping(value = "/customers/{id}")
    public CustomerDTO getCustomer(@PathVariable("id") Long id,
                                   @RequestParam(name = "onlyActiveSubscriptions", defaultValue = "true") Boolean onlyActiveSubscriptions);
}



