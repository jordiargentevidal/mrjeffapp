package mrjeffapp.service.recommendation.service;

import mrjeffapp.service.recommendation.dto.ProductDTO;
import mrjeffapp.service.recommendation.enums.RecommendationType;

import java.util.List;

public interface RecommendationService {

    List<ProductDTO> getRecommendationsByTypeAndCustomer(RecommendationType recommendationType, Long customerId);

}
