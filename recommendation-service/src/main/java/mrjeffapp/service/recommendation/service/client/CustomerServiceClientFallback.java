package mrjeffapp.service.recommendation.service.client;

import lombok.extern.slf4j.Slf4j;
import mrjeffapp.service.recommendation.dto.CustomerDTO;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CustomerServiceClientFallback implements CustomerServiceClient {
    @Override
    public CustomerDTO getCustomer(Long id, Boolean onlyActiveSubscriptions) {
        log.error("calling fallback getCustomer(" + id + "," + onlyActiveSubscriptions.toString() + ")");
        return CustomerDTO.builder().build();
    }
}
