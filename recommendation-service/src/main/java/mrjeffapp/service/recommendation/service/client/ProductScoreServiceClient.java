package mrjeffapp.service.recommendation.service.client;

import mrjeffapp.service.recommendation.dto.ScoreDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(name = "product-score-service", fallback = ProductScoreServiceFallback.class)
public interface ProductScoreServiceClient {

    @GetMapping(value = "/scores")
    public List<ScoreDTO> getScores();

}

