package mrjeffapp.service.recommendation.service.client;

import lombok.extern.slf4j.Slf4j;
import mrjeffapp.service.recommendation.dto.ScoreDTO;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class ProductScoreServiceFallback implements ProductScoreServiceClient {

    @Override
    public List<ScoreDTO> getScores() {
        log.error("calling fallback getScores()");
        return new ArrayList<>();
    }

    ;

}
