package mrjeffapp.service.recommendation.enums;

public enum ProductType {
    SUBSCRIPTION, SERVICE;
}
