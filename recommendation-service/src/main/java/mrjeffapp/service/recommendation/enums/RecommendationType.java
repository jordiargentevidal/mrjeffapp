package mrjeffapp.service.recommendation.enums;

public enum RecommendationType {
    SCORE, HISTORICAL, PERSONA
}
