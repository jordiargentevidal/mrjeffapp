package mrjeffapp.service.recommendation.enums;

public enum PersonaCode {
    JUNIOR, SENIOR
}