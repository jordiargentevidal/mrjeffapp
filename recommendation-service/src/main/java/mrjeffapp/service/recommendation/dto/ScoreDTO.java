package mrjeffapp.service.recommendation.dto;

import lombok.Builder;
import lombok.Data;
import mrjeffapp.service.recommendation.enums.PersonaCode;

@Data
@Builder
public class ScoreDTO {

    private Long id;
    private Integer score;
    private Double averageScore;
    private PersonaCode personaCode;
    private ProductDTO product;


}
