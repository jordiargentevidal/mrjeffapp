package mrjeffapp.service.recommendation.dto;

import lombok.Builder;
import lombok.Data;
import mrjeffapp.service.recommendation.enums.PersonaCode;

import java.util.List;

@Data
@Builder
public class CustomerDTO {

    private Long id;
    private String name;
    private PersonaCode personaCode;
    private List<SubscriptionDTO> subscriptions;


}
