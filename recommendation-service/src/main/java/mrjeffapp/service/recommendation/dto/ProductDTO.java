package mrjeffapp.service.recommendation.dto;

import lombok.Builder;
import lombok.Data;
import mrjeffapp.service.recommendation.enums.ProductType;

@Data
@Builder
public class ProductDTO {

    private Long id;
    private String vertical;
    private String description;
    private Double price;
    private ProductType productType;

}
