package mrjeffapp.service.recommendation.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SubscriptionDTO {

    private Long id;
    private Boolean active;
    private ProductDTO product;

}
